﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Diagnostics;

namespace ServerLogger
{
    public class ServerLog
    {
        private EventLog m_EventLog;

        public ServerLog(string _applicationName, string _applicationType)
        {
            if ((_applicationName != null) && (_applicationName.Length > 0) && (_applicationType != null) && (_applicationType.Length > 0))
            {
                if (!EventLog.SourceExists(_applicationName))
                {
                    EventLog.CreateEventSource(_applicationName, _applicationType);
                    new ServerLog(_applicationName, _applicationType);
                }
                else
                {
                    m_EventLog = new EventLog();
                    m_EventLog.Source = _applicationName;
                }
            }
            else
            {
                throw new Exception("ApplicationName or ApplicationType cant't be emty!");
            }
        }

        
        public void createInfoLog(string _infoMessage, int _eventID)
        {
            if (_eventID != null)
            {
                m_EventLog.WriteEntry(_infoMessage, EventLogEntryType.Information, _eventID);
            }
            else
            {
                m_EventLog.WriteEntry(_infoMessage, EventLogEntryType.Information);
            }
        }

        public void createWarningLog(string _waringMessage, int _eventID)
        {
            if (_eventID != null)
            {
                m_EventLog.WriteEntry(_waringMessage, EventLogEntryType.Warning, _eventID);
            }
            else
            {
                m_EventLog.WriteEntry(_waringMessage, EventLogEntryType.Warning);
            }
        }

        public void createErrorLog(string _errorMessage, int _eventID)
        {
            if (_eventID != null)
            {
                m_EventLog.WriteEntry(_errorMessage, EventLogEntryType.Error, _eventID);
            }
            else
            {
                m_EventLog.WriteEntry(_errorMessage, EventLogEntryType.Error);
            }
        }

        public void createLoginSuccessLog(string _successMessage, int _eventID)
        {
            if (_eventID != null)
            {
                m_EventLog.WriteEntry(_successMessage, EventLogEntryType.SuccessAudit, _eventID);
            }
            else
            {
                m_EventLog.WriteEntry(_successMessage, EventLogEntryType.SuccessAudit);
            }
        }

        public void createLoginFailureLog(string _failureMessage, int _eventID)
        {
            if (_eventID != null)
            {
                m_EventLog.WriteEntry(_failureMessage, EventLogEntryType.FailureAudit, _eventID);
            }
            else
            {
                m_EventLog.WriteEntry(_failureMessage, EventLogEntryType.Error);
            }
        }
    }
}